create extension if not exists pgcrypto;

create table produk (
    id varchar(36) DEFAULT gen_random_uuid(),
    kode varchar(50) not null,
    nama varchar(100) not null,
    primary key (id),
    unique (kode)
);

create table tagihan (
    id varchar(36) DEFAULT gen_random_uuid(),
    id_produk varchar(36)  not null,
    nomor_pelanggan varchar(100) not null,
    nama_pelanggan varchar(255) not null,
    periode_tagihan date not null,
    jatuh_tempo date not null,
    nilai decimal(19,2) not null,
    lunas boolean not null,
    keterangan varchar(255),
    primary key (id), 
    foreign key (id_produk) references produk(id),
    unique(id_produk, periode_tagihan)
);