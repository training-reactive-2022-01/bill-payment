package com.artivisi.training.reactive.billpayment.dao;

import com.artivisi.training.reactive.billpayment.entity.Produk;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import reactor.core.publisher.Mono;

public interface ProdukDao extends ReactiveCrudRepository<Produk, String> {
    Mono<Produk> findByKode(String kode);
}
