package com.artivisi.training.reactive.billpayment.entity;

import java.math.BigDecimal;
import java.time.LocalDate;

import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;

import lombok.Data;

@Data
public class Tagihan {
    @Id
    private String id;

    @Transient
    private Produk produk;
    private String nomorPelanggan;
    private String namaPelanggan;
    private LocalDate periodeTagihan;
    private LocalDate jatuhTempo;
    private BigDecimal nilai;
    private Boolean lunas;
    private String keterangan;
}
