package com.artivisi.training.reactive.billpayment.controller;

import com.artivisi.training.reactive.billpayment.dao.ProdukDao;
import com.artivisi.training.reactive.billpayment.entity.Produk;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/api")
public class ProdukController {

    @Autowired private ProdukDao produkDao;

    @GetMapping("/produk/{id}")
    public Mono<ResponseEntity<Produk>> cariProdukById(@PathVariable String id){
        return produkDao.findById(id)
        .map(p -> ResponseEntity.ok().body(p))
        .defaultIfEmpty(ResponseEntity.notFound().build())
        ;
    }

    @GetMapping("/produk/")
    public Flux<Produk> cariSemuaProduk(){
        return produkDao.findAll();
    }
}
