package com.artivisi.training.reactive.billpayment.dao;

import com.artivisi.training.reactive.billpayment.entity.Produk;
import com.artivisi.training.reactive.billpayment.entity.Tagihan;

import reactor.core.publisher.Flux;

public interface CustomTagihanDao {
    Flux<Tagihan> cariTagihanByNomorPelangganDanProduk(String nomorPelanggan, Produk produk);
}
