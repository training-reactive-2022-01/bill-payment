package com.artivisi.training.reactive.billpayment.dao;

import com.artivisi.training.reactive.billpayment.entity.Tagihan;

import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import reactor.core.publisher.Flux;

public interface TagihanDao extends 
    ReactiveCrudRepository<Tagihan, String>, 
    CustomTagihanDao {
    
    @Query("select * from tagihan where id_produk = :idProduk")
    Flux<Tagihan> findByIdProduk(String idProduk);
    Flux<Tagihan> findByNomorPelanggan(String nomor);
}
