package com.artivisi.training.reactive.billpayment.dao;

import java.math.BigDecimal;
import java.time.LocalDate;

import com.artivisi.training.reactive.billpayment.entity.Produk;
import com.artivisi.training.reactive.billpayment.entity.Tagihan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.r2dbc.core.DatabaseClient;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

@Slf4j
public class CustomTagihanDaoImpl implements CustomTagihanDao {

    private static final String SQL_CARI_TAGIHAN_BY_PELANGGAN_DAN_PRODUK = 
        "select tagihan.*, produk.kode as kode_produk, produk.nama as nama_produk " +
        "from tagihan inner join produk on produk.id = tagihan.id_produk "+
        "where id_produk = :produk and nomor_pelanggan = :pelanggan";

    @Autowired private DatabaseClient databaseClient;

    @Override
    public Flux<Tagihan> cariTagihanByNomorPelangganDanProduk(String nomorPelanggan, Produk produk) {
        log.debug("Mencari tagihan untuk pelanggan {} dan produk {}", 
            nomorPelanggan, produk.toString());  

        return 
        databaseClient.sql(SQL_CARI_TAGIHAN_BY_PELANGGAN_DAN_PRODUK)
        .bind("produk", produk.getId())
        .bind("pelanggan", nomorPelanggan)
        .map(row -> {
            Produk p = new Produk();
            p.setId(row.get("id_produk", String.class));
            p.setKode(row.get("kode_produk", String.class));
            p.setNama(row.get("nama_produk", String.class));

            Tagihan t = new Tagihan();
            t.setId(row.get("id", String.class));
            t.setProduk(p);
            t.setNomorPelanggan(row.get("nomor_pelanggan", String.class));
            t.setNamaPelanggan(row.get("nama_pelanggan", String.class));
            t.setJatuhTempo(row.get("jatuh_tempo", LocalDate.class));
            t.setPeriodeTagihan(row.get("periode_tagihan", LocalDate.class));
            t.setNilai(row.get("nilai", BigDecimal.class));
            t.setLunas(row.get("lunas", Boolean.class));
            t.setKeterangan(row.get("keterangan", String.class));
            return t;
        }).all();
    }
    
}
