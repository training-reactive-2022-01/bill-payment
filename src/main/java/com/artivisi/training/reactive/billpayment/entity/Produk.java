package com.artivisi.training.reactive.billpayment.entity;

import org.springframework.data.annotation.Id;

import lombok.Data;

@Data
public class Produk {
    @Id
    private String id;
    private String kode;
    private String nama;
}
