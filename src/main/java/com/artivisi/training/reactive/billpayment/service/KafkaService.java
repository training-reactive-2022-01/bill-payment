package com.artivisi.training.reactive.billpayment.service;

import com.artivisi.training.reactive.billpayment.dao.TagihanDao;
import com.artivisi.training.reactive.billpayment.dto.CreateVaRequest;
import com.artivisi.training.reactive.billpayment.dto.PaymentResponse;
import com.artivisi.training.reactive.billpayment.entity.Tagihan;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;
import reactor.core.scheduler.Schedulers;

@Service @Slf4j
public class KafkaService {
    @Autowired private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired private ObjectMapper objectMapper;

    @Autowired private TagihanDao tagihanDao;

    @Value("${va.topic.create.request}") 
    private String topicCreateVaRequest;

    public void createVaTagihan(Tagihan tagihan) {
        CreateVaRequest vaRequest = new CreateVaRequest();
        vaRequest.setNomor(tagihan.getNomorPelanggan());
        vaRequest.setNama(tagihan.getNamaPelanggan());
        vaRequest.setNilai(tagihan.getNilai());
        vaRequest.setKeterangan(tagihan.getKeterangan());
        log.info("Membuat virtual account untuk membayar tagihan {}", vaRequest.toString());
        try {
            String strRequest = objectMapper.writeValueAsString(vaRequest);
            kafkaTemplate.send(topicCreateVaRequest, strRequest);
        } catch (JsonProcessingException e) {
            log.error(e.getMessage(), e);            
        }
    }

    @KafkaListener(topics = "${va.topic.payment.response}")
    public Mono<Void> handlePayment(String msg){
        return Mono.fromCallable(() -> {
                PaymentResponse pr = objectMapper.readValue(msg, PaymentResponse.class);
                log.info("Terima pembayaran tagihan {}", pr.toString());
                return pr;
            }).subscribeOn(Schedulers.boundedElastic())
            .flatMapMany(p -> tagihanDao.findByNomorPelanggan(p.getNomor()))
            .reduce((acc, t) -> {
                t.setLunas(true);
                tagihanDao.save(t);
                return t;
        }).then();
    }
}
