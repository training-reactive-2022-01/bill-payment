package com.artivisi.training.reactive.billpayment.controller;

import com.artivisi.training.reactive.billpayment.dao.ProdukDao;
import com.artivisi.training.reactive.billpayment.dao.TagihanDao;
import com.artivisi.training.reactive.billpayment.entity.Tagihan;
import com.artivisi.training.reactive.billpayment.service.KafkaService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import reactor.core.publisher.Flux;

@RestController
@RequestMapping("/api")
public class TagihanController {

    @Autowired private ProdukDao produkDao;
    @Autowired private TagihanDao tagihanDao;

    @Autowired private KafkaService kafkaService;

    @GetMapping("/tagihan/{nomor}/")
    public Flux<Tagihan> inquiry(@PathVariable String nomor, @RequestParam String produk) {
        return produkDao.findByKode(produk)
        .flatMapMany(p -> 
            tagihanDao.cariTagihanByNomorPelangganDanProduk(nomor, p))
        .map(t -> {
            kafkaService.createVaTagihan(t);
            return t;
        }); 
    }
}
