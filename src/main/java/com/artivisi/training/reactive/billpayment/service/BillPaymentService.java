package com.artivisi.training.reactive.billpayment.service;

import com.artivisi.training.reactive.billpayment.dao.ProdukDao;
import com.artivisi.training.reactive.billpayment.dao.TagihanDao;
import com.artivisi.training.reactive.billpayment.entity.Tagihan;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import reactor.core.publisher.Flux;

@Service
public class BillPaymentService {

    @Autowired private ProdukDao produkDao;
    @Autowired private TagihanDao tagihanDao;

    public Flux<Tagihan> cariTagihanByKodeProdukDanNomorPelanggan(String kodeProduk, String nomorPelanggan){
        return produkDao.findByKode(kodeProduk)
        .flatMapMany(produk -> tagihanDao.cariTagihanByNomorPelangganDanProduk(nomorPelanggan, produk));
    }
}
