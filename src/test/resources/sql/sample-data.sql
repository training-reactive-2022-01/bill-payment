insert into produk (id, kode, nama)
values ('p001', 'PLN-POSTPAID', 'PLN Pasca Bayar');

insert into tagihan (id, id_produk, nomor_pelanggan, nama_pelanggan, periode_tagihan, jatuh_tempo, nilai, lunas, keterangan)
values ('t001', 'p001', '1234567890100', 'Pelanggan 100', '2022-01-01', '2022-02-20', 125000.00, false, 'Tagihan Januari 2022');