package com.artivisi.training.reactive.billpayment.dao;

import com.artivisi.training.reactive.billpayment.entity.Produk;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class TagihanDaoTests {
    @Autowired private TagihanDao tagihanDao;

    //@Test
    public void testCariTagihanByPelangganDanProduk(){
        String nomorPelanggan = "1234567890100";
        Produk produk = new Produk();
        produk.setId("p001");
        tagihanDao.cariTagihanByNomorPelangganDanProduk(nomorPelanggan, produk)
        .map(t -> {
            System.out.println("Tagihan : "+t.toString());
            return t;
        }).then().block();
    }

    @Test
    public void testCariTagihanByProduk(){
        tagihanDao.findByIdProduk("p001")
        .map(t -> {
            System.out.println("Tagihan : "+t.toString());
            return t;
        }).then().block();
    }
}
