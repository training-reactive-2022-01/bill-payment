package com.artivisi.training.reactive.billpayment.dao;

import java.util.List;

import com.artivisi.training.reactive.billpayment.entity.Produk;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class ProdukDaoTests {
    @Autowired private ProdukDao produkDao;

    //@Test
    public void testInsertProduk(){
        Produk p = new Produk();
        p.setKode("BPJS-KES");
        p.setNama("BPJS Kesehatan");

        produkDao.save(p).block();
    }

    @Test
    public void testFindAllProduk(){
        produkDao.findAll()
        .map(x -> {
            System.out.println("Produk : "+x.toString());
            return x;
        }).then().block();

        List<Produk> dataProduk = produkDao.findAll().collectList().block();
        System.out.println("Jumlah produk : "+dataProduk.size());
    }
}
