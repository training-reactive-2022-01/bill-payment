package com.artivisi.training.reactive.billpayment.service;

import java.util.List;

import com.artivisi.training.reactive.billpayment.entity.Tagihan;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
public class BillPaymentServiceTests {
    @Autowired private BillPaymentService billPaymentService;

    @Test
    public void testCariTagihanByKodeProdukDanNomorPelanggan(){
        String kodeProduk = "PLN-POSTPAID";
        String nomorPelanggan = "123456789010";

        List<Tagihan> daftarTagihan = billPaymentService
            .cariTagihanByKodeProdukDanNomorPelanggan(kodeProduk, nomorPelanggan)
            .map(t -> {
                System.out.println("Tagihan : "+t.toString());
                return t;
            }).collectList().block();

        System.out.println("Jumlah tagihan : "+daftarTagihan.size());
    }
}
